package com.camunda.fox.showcase.invoice.de.tasklist;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.User;

@Named
@SessionScoped
public class TaskListController implements Serializable {

  private static final long serialVersionUID = 1L;

  @Inject
  private IdentityService identityService;

  public List<User> getUsersList() {
    return identityService.createUserQuery().list();
  }
 
}
