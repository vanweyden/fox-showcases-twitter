package com.camunda.fox.showcase.invoice.de;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

import org.activiti.cdi.BusinessProcess;

@Model
public class ApproveInvoiceController {

  private boolean approved;

  @Inject
  private BusinessProcess businessProcess;

  public boolean isApproved() {
    return approved;
  }

  public void setApproved(boolean approved) {
    this.approved = approved;
    businessProcess.setVariable("approved", approved);
  }
}
