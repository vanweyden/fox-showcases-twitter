package com.camunda.fox.example.versicherung.controller;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;

@Named
public class TaskListController {
  
  @Inject 
  private TaskService taskService;
    
  public List<Task> getTaskList(String name) {
    return taskService.createTaskQuery()
      .taskAssignee(name)
      .list();
  }
}
