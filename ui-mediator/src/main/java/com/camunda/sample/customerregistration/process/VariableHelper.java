package com.camunda.sample.customerregistration.process;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.cdi.BusinessProcess;


public class VariableHelper {
  
  @Inject
  private BusinessProcess businessProcess;
  
  @Produces
  @Named("coreResultDelivereyAsynchronously")
  public Boolean getIsCoreResultDelivereyAsynchronously() {
    return (businessProcess.getVariable("scoreResultCorrelationKey")!=null);
  }


}
