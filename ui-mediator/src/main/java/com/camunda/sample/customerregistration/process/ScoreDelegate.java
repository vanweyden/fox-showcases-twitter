package com.camunda.sample.customerregistration.process;

import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.InitialContext;

import org.activiti.cdi.BusinessProcess;
import org.activiti.cdi.impl.util.ProgrammaticBeanLookup;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.Execution;

import com.camunda.sample.customerregistration.CallbackService;
import com.camunda.sample.customerregistration.Customer;
import com.camunda.sample.customerregistration.CustomerService;
import com.camunda.sample.customerregistration.ScoreResult;
import com.camunda.sample.customerregistration.ScoringService;

@Named
public class ScoreDelegate {
  
  @Inject
  @Named("customer")
  private Customer customer;
 
  @Inject
  private ScoringService scoringService;

  @Inject
  private BusinessProcess businessProcess;
  
  @Inject
  private RuntimeService runtimeService;
  
  @Inject
  private CallbackService callbackService;
  
  public void execute() throws Exception {
    System.out.println("Request scoring for " + customer);
    ScoreResult scoreResult = scoringService.getScoringForCustomer(customer);
    if (scoreResult.isAsynchronousAnswer()) {
      System.out.println("score requested, but will be delivered asynchronously after " + customer.getZipCode() + " milliseconds with correlation key " + scoreResult.getAsynchronousCorrelationKey());
      businessProcess.setVariable("scoreResultCorrelationKey", scoreResult.getAsynchronousCorrelationKey());
      callbackService.triggerAsynchronousCallback(customer, scoreResult.getAsynchronousCorrelationKey());      
    }
    else {
      System.out.println("score received immidately");
      customer.setScoringPoints(scoreResult.getScoringPoints());
    }    
  }

}
